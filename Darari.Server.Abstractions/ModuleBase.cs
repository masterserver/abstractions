﻿namespace Darari.Server.Abstractions;

public abstract class ModuleBase
{
    protected IMessageDistributor MessageDistributor;
    protected IConnectionsService ConnectionsService;
    protected IModulesService ModulesService;
    

    public ModuleBase(IMessageDistributor messageDistributor, IConnectionsService connectionsService, IModulesService modulesService)
    {
        MessageDistributor = messageDistributor;
        ConnectionsService = connectionsService;
        ModulesService = modulesService;
    }
}