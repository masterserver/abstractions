﻿namespace Darari.Server.Abstractions;

public interface IModulesService
{
    public T? GetModule<T>() where T: ModuleBase;
}