﻿using System.Net;

namespace Darari.Server.Abstractions;

public interface IClient
{
    public uint ID { get; }

    public IPAddress Address { get; }
    
    public ConnectionState State { get; }

    public void Send<T>(uint moduleId, uint packageId, T message);

    public void Disconnect(string reason = "");
}