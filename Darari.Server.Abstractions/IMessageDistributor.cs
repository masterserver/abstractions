﻿using Darari.Serialization.Abstractions;

namespace Darari.Server.Abstractions;

public interface IMessageDistributor
{
    void RegisterHandler<T>(uint moduleId, uint messageId, Action<IClient, uint, T> callback) where T: ISerializable;

    void RegisterHandlerUnmanaged<T>(uint moduleId, uint messageId, Action<IClient, uint, T> callback) where T : unmanaged;

    void RemoveMessageFromHandler(uint moduleId, uint messageId);

    void RemoveHandler(uint moduleId);
}