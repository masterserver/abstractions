﻿namespace Darari.Server.Abstractions;

public enum ConnectionState
{
    None = 0,
    Connecting = 1,
    Connected = 2,
    Disconnecting = 3,
    Disconnected = 4
}