﻿namespace Darari.Server.Abstractions;

public interface IConnectionsService
{
    public event Action<IClient> OnClientConnect;
    
    public event Action<IClient> OnClientDisconnect; 

    public IReadOnlyList<IClient> Connections { get; }

    public void Broadcast<T>(uint moduleId, uint messageId, T package);
}